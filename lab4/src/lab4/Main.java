/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab4;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class Main {
    
    public static void main(String[] args) {
        
        List<Language> myLanguages = new ArrayList<Language>();
        
        AzerbaijaniLanguage az = new AzerbaijaniLanguage("Azerbaijani", 1);
        RussianLanguage ru = new RussianLanguage("Russian", 2);
        EnglishLanguage en = new EnglishLanguage("English", 3);
        ItalianLanguage it = new ItalianLanguage("Italian", 4);
        
        myLanguages.add(az);
        myLanguages.add(ru);
        myLanguages.add(en);
        myLanguages.add(it);
        
        for ( Language language : myLanguages )
        {
            System.out.println("Language Name: " + language.getLanguagName());
            System.out.println("\tLanguageId: " + language.getLanguageId());
            System.out.print("\t");
            language.greet();
            System.out.print("\t");
            language.sayGoodBye();
            System.out.println("");
        }
        
    }
    
}
