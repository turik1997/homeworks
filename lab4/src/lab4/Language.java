/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab4;

/**
 *
 * @author Admin
 */
public abstract class Language {
    
    private String languagName;
    private int languageId;

    public Language(String languagName, int languageId) {
        this.languagName = languagName;
        this.languageId = languageId;
    }
    
    
    public abstract void sayGoodBye();
    public abstract void greet();

    public String getLanguagName() {
        return languagName;
    }

    public int getLanguageId() {
        return languageId;
    }
    
    
    
}
