/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab4;

/**
 *
 * @author Admin
 */
public class AzerbaijaniLanguage extends Language {

    public AzerbaijaniLanguage(String languagName, int languageId) {
        super(languagName, languageId);
    }

    @Override
    public void sayGoodBye() {
        System.out.println("Gələn görüşlərədək!");
    }

    @Override
    public void greet() {
        System.out.println("Salam!");
    }
    
    
    
}
