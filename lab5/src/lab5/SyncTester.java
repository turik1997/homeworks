/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class SyncTester {
    
    int count = 0;
    private /*synchronized*/ void incrementCount(){
        count++;
    }
    
    
    public void test(){
        Runnable incrementor = new Runnable() {
            @Override
            public void run() {
                for ( int i = 0; i < 10000; i++ )
                        incrementCount();
            }
        };
        
        Thread thread1 = new Thread(incrementor);
        Thread thread2 = new Thread(incrementor);
        
        thread1.start();
        thread2.start();
        
        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException ex) {}
        
        System.out.println("Count is " + count + ". Must be 20000");
        
    }
    
    
        
}
