/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class Main {
    
    public static void main(String[] args){
        
        //"join" method's usage is also shown in testSleepingThread().
        //Waits for 5 seconds
        testSleepingThread();
        
        testYieldingThread();
        
        testSynchronization();
    }
    
    private static void testSleepingThread(){
        SleepingThread sleepingThread = new SleepingThread();
        sleepingThread.start();
        
        try {
            sleepingThread.join();
        } catch (InterruptedException ex) {}
    }
    
    private static void testYieldingThread(){
        YieldingThread yieldingThread = new YieldingThread();
        NotYieldingThread notYieldingThread = new NotYieldingThread();
        
        
        
        
        yieldingThread.start();
        try {
            Thread.sleep(1);
        } catch (InterruptedException ex) {}
        
        notYieldingThread.start();
    }
    
    private static void testSynchronization(){
        SyncTester syncTester = new SyncTester();
        syncTester.test();
    }
}
