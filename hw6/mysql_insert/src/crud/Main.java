/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class Main {
    
    private static String dbName, user, pass;
    private static Connection connection = null;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("JDBC connection testing...");
        
        connection = getDBConnection();
        
        if (connection != null) {
		System.out.println("Success!");
	} else {
		System.out.println("Failed to make connection!");
                return;
	}
        
        try {
            createTable();
            insertIntoTable("turik1997", "my_super_pass");
            insertIntoTable("anonymous", "123");
            connection.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            return;
        }
        
    }

    private static void readDbInfo() {
        Scanner sc = new Scanner(System.in);
        
        System.out.print("Enter your DB name: ");
        dbName = sc.nextLine();
        System.out.print("Enter username: ");
        user = sc.nextLine();
        System.out.print("Enter password: ");
        pass = sc.nextLine();
    }
    
    private static void createTable() throws SQLException {

		PreparedStatement preparedStatement = null;

		String createTableSQL = "CREATE TABLE USER("
				+ "USER_ID int NOT NULL AUTO_INCREMENT, "
				+ "USERNAME VARCHAR(20) NOT NULL, "
                                + "EMAIL VARCHAR(35) NOT NULL, "
				+ "BIRTHDAY DATE, " + "PRIMARY KEY (USER_ID) "
				+ ")";

		try {
			
			preparedStatement = connection.prepareStatement(createTableSQL);

			System.out.println(createTableSQL);

			// execute create SQL stetement
			preparedStatement.executeUpdate();

			System.out.println("Table \"dbuser\" is created!");

		} catch (SQLException e) {

			System.out.println(e.getMessage());

		} finally {

			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}

	}
    
    private static Connection getDBConnection() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            System.out.println("MySQL JDBC Driver not found!");
            ex.printStackTrace();
            return null;
        }
        
        System.out.println("MySQL JDBC Driver Registered!");

        readDbInfo();
        
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + dbName, user, pass);
        } catch (SQLException ex) {
            System.out.println("Connection Failed! Check output console");
            ex.printStackTrace();
            return null;
        }
        
        
        return connection;
    }
    
    private static void insertIntoTable(String username, String pass) throws SQLException {

		PreparedStatement preparedStatement = null;

		String insertTableSQL = "INSERT INTO USER"
				+ "(USERNAME, EMAIL, BIRTHDAY) VALUES"
				+ "(?,?,?)";

		try {
			preparedStatement = connection.prepareStatement(insertTableSQL);

			preparedStatement.setString(1, username);
			preparedStatement.setString(2, pass);
			preparedStatement.setTimestamp(3, new Timestamp(new Date().getTime()));

			// execute insert SQL stetement
			preparedStatement.executeUpdate();

			System.out.println("User '" + username + "' is inserted into " + dbName.toUpperCase() + ".USER table!");

		} catch (SQLException e) {

			System.out.println(e.getMessage());

		} finally {

			if (preparedStatement != null) {
				preparedStatement.close();
			}

		}

	}
    
}
