package student_project;

import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Turkhan Badalov
 */
public class Student {
    
    private String name;
    private String major;
    private String email;
    private Integer nCredits;
    private Double gpa;
    
    
    public Student(){}

    public Student(String name) {
        this.name = name;
    }

    public Student(String name, String major, String email, int credits, double gpa) {
        this.name = name;
        this.major = major;
        this.email = email;
        this.nCredits = nCredits;
        this.gpa = gpa;
    }
    
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getCredits() {
        return nCredits;
    }

    public void setCredits(int credits) {
        this.nCredits = credits;
    }

    public double getGpa() {
        return gpa;
    }

    public void setGpa(double gpa) {
        this.gpa = gpa;
    }
    
    public void addToCredits(int c){
        this.nCredits += c;
    }

    @Override
    public String toString() {
        
        StringBuilder result = new StringBuilder();
        Field[] fields = this.getClass().getDeclaredFields();
        for ( Field field : fields )
        {
            Object fieldValue = null;
            try
            {
                if ( (fieldValue = field.get(this)) == null )
                    continue;
            } catch (Exception ex) {}
            
            result.append(field.getName() + ":\t");
            if ( fieldValue instanceof String )
                result.append((String)fieldValue);
            else
            if ( fieldValue instanceof Integer )
                result.append((Integer)fieldValue);
            else
            if ( fieldValue instanceof Double )
                result.append((Double)fieldValue);
            result.append("\n");
        }
        
        return result.toString();
    }
    
    
}
