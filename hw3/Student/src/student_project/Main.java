package student_project;

import java.util.Scanner;

/**
 *
 * @author Turkhan Badalov
 */
public class Main {
    public static void main(String[] args) {
        Student dummyStudent = new Student("Turkhan");
        dummyStudent.setEmail("badalov.turxan@gmail.com");
        dummyStudent.setCredits(70);
        System.out.println(dummyStudent.toString());
        
        dummyStudent.setMajor("Android programming and Algorithms");
        dummyStudent.addToCredits(10);
        System.out.println(dummyStudent.toString()); //additional field with major value will be printed
    }
    
}
