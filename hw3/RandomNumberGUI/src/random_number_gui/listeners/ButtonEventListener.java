package random_number_gui.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.JLabel;
/**
 *
 * @author Turkahn Badalov
 */
public class ButtonEventListener implements ActionListener {
    
    private JLabel label;
    private Random random;
    public ButtonEventListener(JLabel label) {
        this.label = label;
        this.random = new Random();
        
    }
    
    
    public void actionPerformed(ActionEvent e) {
            label.setText(Integer.toString(random.nextInt(100) + 1));

    }
}