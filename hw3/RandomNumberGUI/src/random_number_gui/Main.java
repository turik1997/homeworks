package random_number_gui;

import java.awt.Dimension;
import java.awt.Toolkit;

/**
 *
 * @author Turkhan Badalov
 */
public class Main {
    public static void main(String[] args) {
        MainFrame app = new MainFrame();
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int horizontalPosition = (int)(dimension.getWidth()/2 - app.getWidth()/2);
        int verticalPosition = (int)(dimension.getHeight()/2 - app.getHeight()/2);
        app.setLocation(horizontalPosition, verticalPosition);
        app.setVisible(true);
    }
}
