package random_number_gui;

import random_number_gui.listeners.ButtonEventListener;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author Turkhan Badalov
 */
public class MainFrame extends JFrame {
    
    JButton btnRandomGenerator = new JButton("Generate random number!");
    JLabel label = new JLabel();
    
    public MainFrame() {
	    super("Random Number Generator");
	    this.setSize(300, 200);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            setLayout(new GridLayout(2, 1));
            label.setHorizontalAlignment(JLabel.CENTER);
            add(label);
            add(btnRandomGenerator);
               
            
	    btnRandomGenerator.addActionListener(new ButtonEventListener(label));
	}
    
}
