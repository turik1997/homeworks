package dogtask;

/**
 *
 * @author Turkhan Badalov
 */
public class Kennel {

    public static void main(String[] args) {
        
        Dog bobik = new Dog("Bobik", 6);
        System.out.println(bobik.toString());
        System.out.println("Age in human years: " + bobik.ageInHumanYears() + "\n");
        
        for ( int i = 0; i < 10; i++ )
        {
            System.out.println(new Dog("Jecky" + i, (int)(10 - Math.random()* 9)).toString());
        }
    }
    
}
