package lab3.part2;

/**
 *
 * @author Turkhan Badalov
 */
public class Circle extends Shape{
    
    private double radius;

    public Circle() {
        radius = 1;
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle(double xPos, double yPos, double radius) {
        super(xPos, yPos);
        this.radius = radius;
    }
    
    @Override
    public double getArea() {
        return Math.PI * radius * radius;
    }
    
    @Override
    public void printShape() {
        System.out.println("This is circle: O. And this is mini circle: o.");
    }

    @Override
    public String toString() {
        return "Circle (" + getXPos() + ", " + getYPos() + ", " + radius + ")";
    }
    
    public boolean isInside(double x, double y){
        return (x-getXPos())*(x - getXPos()) + (y - getYPos())*(y-getYPos()) < radius*radius;
    }
    
}
