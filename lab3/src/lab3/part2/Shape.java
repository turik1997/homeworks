/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab3.part2;

/**
 *
 * @author Admin
 */
public abstract class Shape {
    
    private double xPos;
    private double yPos;

    public Shape() {
        xPos = yPos = 0;
    }
    
    public Shape(double xPos, double yPos) {
        this.xPos = xPos;
        this.yPos = yPos;
    }
    
    void setPos(double x, double y){
        this.xPos = x;
        this.yPos = y;
    }

    public double getXPos() {
        return xPos;
    }

    public double getYPos() {
        return yPos;
    }

    @Override
    public String toString() {
        return "Shape (" + xPos + ", " + yPos + ")";
    }
    
    public abstract double getArea();
    public abstract void printShape();
    public abstract boolean isInside(double x, double y);
}
