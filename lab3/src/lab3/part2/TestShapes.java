package lab3.part2;

/**
 *
 * @author Turkhan Badalov
 */
public class TestShapes {
    
    private static Rectangle defaultRectangle;
    private static Rectangle specifiedRectangle;
    private static Circle defaultCircle;
    private static Circle specifiedCircle;
    
    public static void main(String[] args){
        
        testRectangles();
        testCircles();
        
        System.out.println(defaultCircle.isInside(0.5, 0.5));
        System.out.println(specifiedRectangle.isInside(2, 3));
    }
    
    public static void testCircles(){
        defaultCircle = new Circle();
        specifiedCircle = new Circle(4, 5, 3);
        
        System.out.println(defaultCircle);
        defaultCircle.printShape();
        
        System.out.println(specifiedCircle);
        specifiedCircle.printShape();
    }
    
    public static void testRectangles(){
        defaultRectangle = new Rectangle();
        specifiedRectangle = new Rectangle(2, 2, 2, 4);
        
        System.out.println(defaultRectangle);
        defaultRectangle.printShape();
        
        System.out.println(specifiedRectangle);
        specifiedRectangle.printShape();
    }
}
