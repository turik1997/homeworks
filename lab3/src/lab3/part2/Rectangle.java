package lab3.part2;

/**
 *
 * @author Turkhan Badalov
 */
public class Rectangle extends Shape{
    
    private double width;
    private double height;

    public Rectangle() {
        width = height = 1;
    }

    public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }

    public Rectangle(double xPos, double yPos, double width, double height) {
        super(xPos, yPos);
        this.width = width;
        this.height = height;
    }
    
    

    @Override
    public double getArea() {
        return width * height;
    }

    @Override
    public void printShape() {
        for ( int i = 0; i < height; i++ )
        {
            for ( int j = 0; j < width; j++ )
            {
                System.out.print("*");
            }
            System.out.print("\n");
        }
    }

    @Override
    public String toString() {
        return "Rectangle (" + getXPos() + ", " + getYPos() + ") - "
                + "(" + (getXPos()+width) + ", " + (getYPos()+height) + ")";
    }

    @Override
    public boolean isInside(double x, double y) {
        return x >= getXPos() && x <= getXPos()+width
                && y >= getYPos() && y <= getYPos()+height;
    }
    
    
    
    
    
}
