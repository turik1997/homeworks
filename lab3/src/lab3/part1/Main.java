package lab3.part1;

/**
 *
 * @author Turkhan Badalov
 */
public class Main {

    public static void main(String[] args) {
        Rectangle defaultRectangle = new Rectangle();
        Rectangle specifiedRectangle = new Rectangle(2, 2, 2, 4);
        
        System.out.println(defaultRectangle);
        defaultRectangle.printShape();
        
        System.out.println(specifiedRectangle);
        specifiedRectangle.printShape();
    }
    
}
