package lab3.part1;

/**
 *
 * @author Turkhan Badalov
 */
public class Rectangle {
    
    private double xPos;
    private double yPos;
    private double width;
    private double height;
    
    public Rectangle(){
        xPos = yPos = 0;
        width = height = 1;
    }

    public Rectangle(double width, double height) {
        this();
        this.width = width;
        this.height = height;
    }

    public Rectangle(double xPos, double yPos, double width, double height) {
        this(width, height);
        this.xPos = xPos;
        this.yPos = yPos;
    }
    
    public void setPos(double x, double y){
        this.xPos = x;
        this.yPos = y;
    }

    public double getxPos() {
        return xPos;
    }

    public double getyPos() {
        return yPos;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }
    
    public double getArea(){
        return height * width;
    }

    @Override
    public String toString() {
        return "Rectangle (" + xPos + ", " + yPos + ") - "
                + "(" + (xPos+width) + ", " + (yPos+height) + ")";
    }
    
    public void printShape(){
        for ( int i = 0; i < height; i++ )
        {
            for ( int j = 0; j < width; j++ )
            {
                System.out.print("*");
            }
            System.out.print("\n");
        }
    }
    
    
    
    
    
    
}
