package primenumberdetector;

import java.util.Scanner;

/**
 *
 * @author Turkhan Badalov
 */
public class PrimeNumberDetector {

    public static void main(String[] args) {
        int n; boolean is_prime = true;
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        int root = (int)Math.sqrt(n);
        for ( int i = 2; i <= root; i++ )
        {
            if ( n % i == 0 )
            {
                is_prime = false;
                break;
            }
        }
        
        System.out.println(n + (is_prime ? " is prime number" : " is NOT prime number"));
    }
    
}
